#!/usr/bin/env python
import os, sys


def open_file(name, size):
    if not os.path.isfile(sys.argv[1]):
        print("This is not a valid file")
        return (False)
    my_file = open(sys.argv[1], "r")
    num_lines = sum(1 for line in my_file)
    my_file.close()
    my_file = open(sys.argv[1], "r")
    for line in my_file:
        if len(line) != num_lines + 1:
            print("have to be a perfect square")
            return (False)
    if (size < 2 or size >= num_lines):
        print ("Size must be larger than 0 and smaller than size of map")
        exit(84)
    my_file.close()
    my_file = open(sys.argv[1], "r")
    my_file_read = my_file.read()
    my_file.close()
    newstr = my_file_read.split('\n')
    return (newstr)

def main():
    if len(sys.argv) < 3:
        print("usage : python %s <map> <size>" % sys.argv[0])
        exit(84)
    size = int(sys.argv[2])
    array = open_file(sys.argv[1], size)
    if not array:
        print("An error occured, sorry")
        exit (84)
    # WRITE YOUR CODE BETWEEN THIS LINE

    # AND THIS ONE

if __name__ == "__main__":
    main()
